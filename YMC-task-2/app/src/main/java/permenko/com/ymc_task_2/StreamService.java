package permenko.com.ymc_task_2;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import static permenko.com.ymc_task_2.PlayerActivity.HIDE_STOP;
import static permenko.com.ymc_task_2.PlayerActivity.SHOW_PAUSE;
import static permenko.com.ymc_task_2.PlayerActivity.SHOW_PLAY;
import static permenko.com.ymc_task_2.PlayerActivity.SHOW_STOP;
import static permenko.com.ymc_task_2.PlayerActivity.UI_RECEIVER;


public class StreamService extends Service implements MediaPlayer.MediaPlayerState {

    public final static String ON_PLAY_PAUSE_CLICK = "ON_PLAY_PAUSE_CLICK";
    public final static String INIT_UI = "INIT_PLAYER_UI";

    private MediaPlayer mediaPlayer;

    private int notificationId = 53152;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (mediaPlayer == null) initPlayer();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null || intent.getAction() == null) {
            return super.onStartCommand(intent, flags, startId);
        }

        if (intent.getAction().equals(ON_PLAY_PAUSE_CLICK)) { //handle play/pause click
            onPlayPauseClick();
        } else if (intent.getAction().equals(INIT_UI)) { //handle ui after activity created
            if (mediaPlayer.getPlayWhenReady()) {
                sendBroadcast(new Intent(UI_RECEIVER).putExtra(UI_RECEIVER, SHOW_PAUSE));
                sendBroadcast(new Intent(UI_RECEIVER).putExtra(UI_RECEIVER, SHOW_STOP));
            } else {
                sendBroadcast(new Intent(UI_RECEIVER).putExtra(UI_RECEIVER, SHOW_PLAY));
                sendBroadcast(new Intent(UI_RECEIVER).putExtra(UI_RECEIVER, SHOW_STOP));
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //update UI
        sendBroadcast(new Intent(UI_RECEIVER).putExtra(UI_RECEIVER, SHOW_PLAY));
        sendBroadcast(new Intent(UI_RECEIVER).putExtra(UI_RECEIVER, HIDE_STOP));
        //stop player
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer = null;
        }
    }

    private void onPlayPauseClick() {
        if (mediaPlayer.getPlayWhenReady()) {
            pausePlayer();
            sendBroadcast(new Intent(UI_RECEIVER).putExtra(UI_RECEIVER, SHOW_PLAY));
        } else {
            startPlayer();
            sendBroadcast(new Intent(UI_RECEIVER).putExtra(UI_RECEIVER, SHOW_PAUSE));
            sendBroadcast(new Intent(UI_RECEIVER).putExtra(UI_RECEIVER, SHOW_STOP));
        }
    }

    private void initPlayer() {
        mediaPlayer = new MediaPlayer(StreamService.this);
        mediaPlayer.setStateListener(this);
    }

    private void startPlayer() {
        mediaPlayer.start();
    }

    private void pausePlayer() {
        if (mediaPlayer == null) return;
        mediaPlayer.pause();
    }

    @Override
    public void buffering() {
        sendNotification(getString(R.string.state_buffering));
    }

    @Override
    public void ended() {
        stopSelf();
    }

    @Override
    public void idle() {
        //ignore
    }

    @Override
    public void preparing() {
        //ignore
    }

    @Override
    public void ready() {
        sendNotification(getString(mediaPlayer.getPlayWhenReady() ? R.string.state_playing : R.string.state_paused));
    }

    @Override
    public void unknown() {
        //ignore
    }

    private void sendNotification(String title) {
        startForeground(notificationId, getNotification(title));
    }

    private Notification getNotification(String contentText) {
        Intent showTaskIntent = new Intent(getApplicationContext(), PlayerActivity.class);
        showTaskIntent.setAction(Intent.ACTION_MAIN);
        showTaskIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        showTaskIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(
                getApplicationContext(),
                0,
                showTaskIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        return new Notification.Builder(getApplicationContext())
                .setContentTitle(getString(R.string.app_name))
                .setContentText(contentText)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(contentIntent)
                .build();
    }
}
