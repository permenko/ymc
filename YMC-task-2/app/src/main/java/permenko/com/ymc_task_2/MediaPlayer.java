package permenko.com.ymc_task_2;

import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecSelector;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;

class MediaPlayer implements ExoPlayer.Listener {

    private final String LOG_TAG = MediaPlayer.class.getSimpleName();
    private ExoPlayer mMediaPlayer;
    private final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private final int BUFFER_SEGMENT_COUNT = 1000;
    private final String USER_AGENT = "Android";
    private Context context;

    MediaPlayer(Context context) {
        this.context = context;
    }

    private void prepareMediaPlayer() {
        mMediaPlayer = ExoPlayer.Factory.newInstance(1);
        mMediaPlayer.addListener(this);
        Allocator allocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
        DataSource dataSource = new DefaultUriDataSource(context, null, USER_AGENT);
        ExtractorSampleSource sampleSource = new ExtractorSampleSource(Uri.parse(BuildConfig.RADIO_URL), dataSource, allocator,
                BUFFER_SEGMENT_COUNT * BUFFER_SEGMENT_SIZE);
        MediaCodecAudioTrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource, MediaCodecSelector.DEFAULT);
        mMediaPlayer.prepare(audioRenderer);
        mMediaPlayer.setPlayWhenReady(true);
    }

    private void releaseMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.setPlayWhenReady(false);
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    void start() {
        if (mMediaPlayer == null) prepareMediaPlayer();
        mMediaPlayer.setPlayWhenReady(true);
    }
    void stop() {
        mediaPlayerState.ended();
        mMediaPlayer.getPlaybackState();
        releaseMediaPlayer();
    }

    void pause() {
        mMediaPlayer.setPlayWhenReady(false);
    }

    boolean getPlayWhenReady() {
        if (mMediaPlayer == null) return false;
        return mMediaPlayer.getPlayWhenReady();
    }

    int getState() {
        return mMediaPlayer.getPlaybackState();
    }

    interface MediaPlayerState {
        void buffering();
        void ended();
        void idle();
        void preparing();
        void ready();
        void unknown();
    }

    private MediaPlayerState mediaPlayerState;

    void setStateListener(MediaPlayerState mediaPlayerState) {
        this.mediaPlayerState = mediaPlayerState;
    }

    @Override
    public void onPlayerStateChanged(boolean b, int playerState) {
        switch(playerState) {
            case ExoPlayer.STATE_BUFFERING:
                mediaPlayerState.buffering();
                break;
            case ExoPlayer.STATE_ENDED:
                mediaPlayerState.ended();
                break;
            case ExoPlayer.STATE_IDLE:
                mediaPlayerState.idle();
                break;
            case ExoPlayer.STATE_PREPARING:
                mediaPlayerState.preparing();
                break;
            case ExoPlayer.STATE_READY:
                mediaPlayerState.ready();
                break;
            default:
                mediaPlayerState.unknown();
                break;
        }
    }

    @Override
    public void onPlayWhenReadyCommitted() {

    }

    @Override
    public void onPlayerError(ExoPlaybackException e) {
        mediaPlayerState.ended();
    }
}
