package permenko.com.ymc_task_2;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static permenko.com.ymc_task_2.StreamService.INIT_UI;
import static permenko.com.ymc_task_2.StreamService.ON_PLAY_PAUSE_CLICK;

public class PlayerActivity extends AppCompatActivity {

    public static final String UI_RECEIVER = "UI_RECEIVER";
    public static final String SHOW_PLAY = "SHOW_PLAY";
    public static final String SHOW_PAUSE = "SHOW_PAUSE";
    public static final String SHOW_STOP = "SHOW_STOP";
    public static final String HIDE_STOP = "HIDE_STOP";

    @BindView(R.id.play_pause)
    Button mPlayPause;
    @BindView(R.id.stop)
    Button mStop;

    @OnClick(R.id.play_pause)
    void onPlayPauseClick() {
        startService(new Intent(this, StreamService.class).setAction(ON_PLAY_PAUSE_CLICK));
    }

    private BroadcastReceiver mUIReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || intent.getAction() == null) return;
            if (intent.getStringExtra(UI_RECEIVER).equals(SHOW_PLAY)) {
                showPlay();
            } else if (intent.getStringExtra(UI_RECEIVER).equals(SHOW_PAUSE)) {
                showPause();
            } else if (intent.getStringExtra(UI_RECEIVER).equals(SHOW_STOP)) {
                showStop();
            } else if (intent.getStringExtra(UI_RECEIVER).equals(HIDE_STOP)) {
                hideStop();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        ButterKnife.bind(this);
        registerReceiver(mUIReceiver, new IntentFilter(UI_RECEIVER));
        initUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mUIReceiver);
    }

    private void initUI() {
        if (!isServiceRunning()) {
            showPlay();
        } else {
            startService(new Intent(this, StreamService.class).setAction(INIT_UI));
        }
    }

    @OnClick(R.id.stop)
    void onStopClick() {
        stopService(new Intent(this, StreamService.class));
    }

    private void showPlay() {
        mPlayPause.setText(getString(R.string.play));
    }

    private void showPause() {
        mPlayPause.setText(getString(R.string.pause));
    }

    private void showStop() {
        mStop.setVisibility(View.VISIBLE);
    }

    private void hideStop() {
        mStop.setVisibility(View.GONE);
    }

    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (StreamService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
