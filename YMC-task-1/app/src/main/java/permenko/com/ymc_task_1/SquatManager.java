package permenko.com.ymc_task_1;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.orhanobut.hawk.Hawk;

public class SquatManager implements SensorEventListener {

	public static final String LOW_SPEED = "LOW_SPEED";
	public static final String HIGH_SPEED = "HIGH_SPEED";

	private boolean mTrainingLowSpeed = false;
	private boolean mTrainingHighSpeed = false;
	private boolean mCalibrationLowSpeed = false;
	private boolean mCalibrationHighSpeed = false;

	private SquatListener mSquatListener;

	private int mSquatCount = 0;

	private long mLastSquatTime = 0;
	private short mFakeMoves = 0;
	private double threshold = 11.91;

	private short mSquatSpeed = 400;

	private SensorManager sensorManager;

	public SquatManager(Context context, SquatListener squatListener) {
		mSquatListener = squatListener;
		sensorManager = (SensorManager) context.getSystemService(context.SENSOR_SERVICE);
	}

	public void registerSensor() {
		registerAccelerometer();
	}

	private boolean registerAccelerometer() {
		if (Hawk.contains(LOW_SPEED) && mTrainingLowSpeed) mSquatSpeed = Hawk.get(LOW_SPEED);
		if (Hawk.contains(HIGH_SPEED) && mTrainingHighSpeed) mSquatSpeed = Hawk.get(HIGH_SPEED);
		Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		return sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
	}

	public void unregisterSensor() {
		sensorManager.unregisterListener(this);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			double x = event.values[0];
			double y = event.values[1];
			double z = event.values[2];
			double v = Math.abs(
					Math.sqrt(
							Math.pow(x, 2)
									+
									Math.pow(y, 2)
									+
									Math.pow(z, 2)
					));

			long currentTime = System.currentTimeMillis();
			long difference = currentTime - mLastSquatTime;

			if ((difference > mSquatSpeed) && (v > threshold)) {

				if ((difference) > 5000 || (mFakeMoves < 5 && mFakeMoves != 0) || mSquatCount == 0) {
					mFakeMoves++;

					if (mFakeMoves >= 5) {
						mFakeMoves = 0;
						mSquatCount++;
						handleCalibration();
						mSquatListener.onChange(mSquatCount);
					}

				} else {
					mSquatCount++;
					handleCalibration();
					mSquatListener.onChange(mSquatCount);

				}

				if ((difference - mSquatSpeed) > 100 && mSquatSpeed < 1024) {
					mSquatSpeed += 10;
				} else if ((difference - mSquatSpeed) < 100 && mSquatSpeed > 356) {
					mSquatSpeed -= 10;
				}

				mLastSquatTime = System.currentTimeMillis();
			}
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int i) {

	}

	private void handleCalibration() {
		if (mSquatCount == 3 && mCalibrationLowSpeed) {
			Hawk.put(LOW_SPEED, mSquatSpeed);
			mSquatListener.onCalibrationFinished();
		}
		if (mSquatCount == 3 && mCalibrationHighSpeed) {
			Hawk.put(HIGH_SPEED, mSquatSpeed);
			mSquatListener.onCalibrationFinished();
		}
	}

	public void setTrackingType(SquatType squatType) {
		if (squatType == SquatType.CALIBRATION_LOW_SPEED) {
			setCalibrationLowSpeed();
		} else if (squatType == SquatType.CALIBRATION_HIGH_SPEED) {
			setCalibrationHighSpeed();
		} else if (squatType == SquatType.TRAINING_LOW_SPEED) {
			setTrainingLowSpeed();
		} else if (squatType == SquatType.TRAINING_HIGH_SPEED) {
			setTrainingHighSpeed();
		}
	}

	private void setCalibrationLowSpeed() {
		mCalibrationLowSpeed = true;

		mCalibrationHighSpeed = false;
		mTrainingHighSpeed = false;
		mTrainingLowSpeed = false;
	}

	private void setCalibrationHighSpeed() {
		mCalibrationHighSpeed = true;

		mCalibrationLowSpeed = false;
		mTrainingHighSpeed = false;
		mTrainingLowSpeed = false;
	}

	private void setTrainingLowSpeed() {
		mTrainingLowSpeed = true;

		mTrainingHighSpeed = false;
		mCalibrationHighSpeed = false;
		mCalibrationLowSpeed = false;
	}

	private void setTrainingHighSpeed() {
		mTrainingHighSpeed = true;

		mTrainingLowSpeed = false;
		mCalibrationHighSpeed = false;
		mCalibrationLowSpeed = false;
	}
}
