package permenko.com.ymc_task_1;

public enum SquatType {
    CALIBRATION_LOW_SPEED,
    CALIBRATION_HIGH_SPEED,
    TRAINING_LOW_SPEED,
    TRAINING_HIGH_SPEED,
}
