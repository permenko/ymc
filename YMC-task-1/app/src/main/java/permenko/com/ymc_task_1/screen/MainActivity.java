package permenko.com.ymc_task_1.screen;

import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.orhanobut.hawk.Hawk;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permenko.com.ymc_task_1.R;
import permenko.com.ymc_task_1.SquatListener;
import permenko.com.ymc_task_1.SquatManager;
import permenko.com.ymc_task_1.SquatType;

import static permenko.com.ymc_task_1.SquatManager.HIGH_SPEED;
import static permenko.com.ymc_task_1.SquatManager.LOW_SPEED;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.high_speed)
    ToggleButton mHighSpeed;
    @BindView(R.id.low_speed)
    ToggleButton mLowSpeed;

    @BindView(R.id.count_high_speed)
    TextView mCountHighSpeed;
    @BindView(R.id.count_low_speed)
    TextView mCountLowSpeed;
    @BindView(R.id.current_count)
    TextView mCurrentCount;

    @BindView(R.id.countdown)
    TextView mCountdown;

    @BindView(R.id.start)
    Button mStart;
    @BindView(R.id.stop)
    Button mStop;
    @BindView(R.id.reset)
    Button mReset;

    private SquatManager mSquatManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        checkTrainingAvailable();
        if (!Hawk.contains(HIGH_SPEED) && !Hawk.contains(LOW_SPEED)) showCalibrationDialog();

        mCountHighSpeed.setText(String.valueOf(Hawk.get("total_count_" + SquatType.TRAINING_HIGH_SPEED.toString(), 0)));
        mCountLowSpeed.setText(String.valueOf(Hawk.get("total_count_" + SquatType.TRAINING_LOW_SPEED.toString(), 0)));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_calibration:
                showCalibrationDialog();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    @OnClick(R.id.high_speed)
    void onHighSpeedClick() {
        mLowSpeed.setChecked(false);
        mHighSpeed.setChecked(true);
    }

    @OnClick(R.id.low_speed)
    void onLowSpeedClick() {
        mHighSpeed.setChecked(false);
        mLowSpeed.setChecked(true);
    }

    @OnClick(R.id.start)
    void onStartClick() {
        if (Hawk.contains(LOW_SPEED) && mLowSpeed.isChecked()) {
            pocketDialog(SquatType.TRAINING_LOW_SPEED);
        }
        if (Hawk.contains(HIGH_SPEED) && mHighSpeed.isChecked()) {
            pocketDialog(SquatType.TRAINING_HIGH_SPEED);
        }
    }

    @OnClick(R.id.reset)
    void onResetClick() {
        Hawk.delete("total_count_" + SquatType.TRAINING_HIGH_SPEED.toString());
        Hawk.delete("total_count_" + SquatType.TRAINING_LOW_SPEED.toString());

        mCountHighSpeed.setText("0");
        mCountLowSpeed.setText("0");
    }

    @OnClick(R.id.stop)
    void onStopClick() {
        checkTrainingAvailable();
        mSquatManager.unregisterSensor();
        mSquatManager = null;
        mStart.setVisibility(View.VISIBLE);
        mReset.setVisibility(View.VISIBLE);
        mCurrentCount.setVisibility(View.GONE);
        mStop.setVisibility(View.GONE);
        checkTrainingAvailable();
    }

    private void checkTrainingAvailable() {
        if (Hawk.contains(LOW_SPEED)) {
            mLowSpeed.setAlpha(1f);
            mLowSpeed.setEnabled(true);
        } else {
            mLowSpeed.setEnabled(false);
        }
        if (Hawk.contains(HIGH_SPEED)) {
            mHighSpeed.setAlpha(1f);
            mHighSpeed.setEnabled(true);
        } else {
            mHighSpeed.setEnabled(false);
        }
    }

    private void showCalibrationDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Регулировка режимов приседаний")
                .setMessage("В приседаниях важно придерживаться одного темпа. SquatCounter поддерживает два режима приседаний - \"Быстрый темп\" и \"Медленный темп\". Пожалуйста сделайте несколько тестовых приседаний для каждого из режимов")
                .setPositiveButton("Быстрый темп", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        pocketDialog(SquatType.CALIBRATION_HIGH_SPEED);
                    }
                })
                .setNegativeButton("Медленный темп", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        pocketDialog(SquatType.CALIBRATION_LOW_SPEED);
                    }
                })
                .show();
    }

    private void pocketDialog(final SquatType squatType) {
        new AlertDialog.Builder(this)
                .setTitle("Положите телефон в карман")
                .setMessage("Подсчет приседаний начнется через 3 секунды")
                .setNegativeButton("Начать", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        mStart.setVisibility(View.GONE);
                        mReset.setVisibility(View.GONE);
                        startTracking(squatType);
                    }
                })
                .show();
    }

    private void startTracking(final SquatType squatType) {
        if (mSquatManager != null) {
            mSquatManager.unregisterSensor();
            mSquatManager = null;
        }
        mLowSpeed.setEnabled(false);
        mHighSpeed.setEnabled(false);
        mCurrentCount.setVisibility(View.GONE);
        mStop.setVisibility(View.GONE);

        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
                mCountdown.setVisibility(View.VISIBLE);
                mCountdown.setText(String.valueOf(millisUntilFinished / 1000));
            }

            public void onFinish() {
                mCountdown.setVisibility(View.GONE);
                mCountdown.setText("0");
                mCurrentCount.setVisibility(View.VISIBLE);
                mCurrentCount.setText(getString(R.string.current_squat_count, 0));
                mStop.setVisibility(View.VISIBLE);
                mSquatManager.registerSensor();
            }
        }.start();

        mSquatManager = new SquatManager(this, new SquatListener() {
            @Override
            public void onChange(final int count) {

                if (squatType == SquatType.TRAINING_HIGH_SPEED) {
                    Hawk.put("total_count_" + squatType.toString(), Hawk.get("total_count_" + squatType.toString(), 0) + 1);
                    mCountHighSpeed.setText(String.valueOf(Hawk.get("total_count_" + squatType.toString(), 0)));
                } else if (squatType == SquatType.TRAINING_LOW_SPEED) {
                    Hawk.put("total_count_" + squatType.toString(), Hawk.get("total_count_" + squatType.toString(), 0) + 1);
                    mCountLowSpeed.setText(String.valueOf(Hawk.get("total_count_" + squatType.toString(), 0)));
                }
                mCurrentCount.setText(getString(R.string.current_squat_count, count));
            }

            @Override
            public void onCalibrationFinished() {
                Toast.makeText(MainActivity.this, "Калибровка завершена", Toast.LENGTH_SHORT).show();
            }
        });

        mSquatManager.setTrackingType(squatType);
    }
}
