package permenko.com.ymc_task_1;

public interface SquatListener {

    void onChange(int count);

    void onCalibrationFinished();

}
